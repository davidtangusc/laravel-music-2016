<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Music Search</title>
</head>
<body>

<p>
    You searched for: <?php echo $artist ?>
</p>

<?php foreach ($songs as $song) : ?>
    <h3>
        <?php echo $song->title ?>
    </h3>
    <span>by</span>
    <span>
        <?php echo $song->artist_name ?>
      </span>
    <p>
        Play Count: <?php echo $song->play_count ?>
    </p>
    <p>
        Price: $<?php echo $song->price ?>
    </p>
<?php endforeach; ?>

</body>
</html>
